package com.infostretch;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.infostretch.model.User;

@SpringBootApplication
@EnableJpaRepositories("com.infostretch.repository")
@ComponentScan("com.infostretch.*")
@EntityScan("com.infostretch.model")
@EnableAspectJAutoProxy
@EnableScheduling
public class ShoppingAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingAdminApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
