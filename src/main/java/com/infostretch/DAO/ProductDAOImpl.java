package com.infostretch.DAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.infostretch.model.Product;
import com.infostretch.repository.ProductRepo;

@Repository
public class ProductDAOImpl implements ProductDAO {

	@Autowired
	ProductRepo productRepo;

	@Override
	public Product addProduct(Product product) {
		return productRepo.save(product);
	}

	@Override
	public Product getProductById(Integer id) {
		return productRepo.findById(id).get();
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public void deleteProduct(Integer id) {
		productRepo.deleteById(id);
	}

	@Override
	public Product updateProduct(Product product, Integer id , MultipartFile file) {
		Product originalProduct = getProductById(id);
		product.setProductId(originalProduct.getProductId());
		try {
			originalProduct.getFile().setFileData(file.getBytes());
			product.setFile(originalProduct.getFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return addProduct(product);
	}

	@Override
	public List<Product> getAllProductsSortByPrice(String sortType) {
		if (sortType.equalsIgnoreCase("Desc"))
			return productRepo.findByOrderByProductPriceDesc();
		else
			return productRepo.findAll(Sort.by(Sort.Direction.ASC, "productPrice"));
	}

	@Override
	public List<Product> getAllProductsByColor(String color) {
		return productRepo.findByProductColor(color);
	}

	@Override
	public List<String> getColors() {
		Set<String> colors  = getAllProducts().stream().map(product -> product.getProductColor()).collect(Collectors.toSet());
		return new ArrayList<String>(colors);
	}

	@Override
	public List<Product> getAllProductsByGender(String gender) {
		return productRepo.findByProductGender(gender);
	}

	@Override
	public List<Product> getAllProductsByWear(String wear) {
		return productRepo.findByProductWear(wear);
	}
	

}
