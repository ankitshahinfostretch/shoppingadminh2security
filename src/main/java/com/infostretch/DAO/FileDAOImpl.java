package com.infostretch.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.infostretch.model.File;
import com.infostretch.repository.FileRepo;

@Repository
public class FileDAOImpl implements FileDAO {

	@Autowired
	FileRepo fileRepo;

	@Override
	public File addFile(byte[] byteObjects) {
		File file = new File();
		file.setFileData(byteObjects);
		return fileRepo.save(file);
	}
}
