package com.infostretch.DAO;

import com.infostretch.model.File;

public interface FileDAO {
	public File addFile(byte[] byteObjects);
}
