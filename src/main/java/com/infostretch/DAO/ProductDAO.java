package com.infostretch.DAO;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.infostretch.model.Product;

public interface ProductDAO {

	public Product addProduct(Product product);

	public Product getProductById(Integer id);

	public List<Product> getAllProducts();

	public void deleteProduct(Integer id);

	public Product updateProduct(Product product, Integer id , MultipartFile file);

	public List<String> getColors();

	// Below Methods are for sorting
	public List<Product> getAllProductsSortByPrice(String sortType);

	// Below Methods are for searching
	public List<Product> getAllProductsByColor(String color);

	public List<Product> getAllProductsByGender(String gender);
	
	public List<Product> getAllProductsByWear(String wear);
}
