package com.infostretch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infostretch.model.Product;

public interface ProductRepo extends JpaRepository<Product, Integer> {

	List<Product> findByOrderByProductPriceDesc();

	List<Product> findByProductColor(String color);

	List<Product> findByProductGender(String gender);

	List<Product> findByProductWear(String gender);
}
