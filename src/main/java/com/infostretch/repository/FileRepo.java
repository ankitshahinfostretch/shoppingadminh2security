package com.infostretch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infostretch.model.File;

public interface FileRepo extends JpaRepository<File, Integer> {

}
