package com.infostretch.service;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.DAO.ProductDAO;
import com.infostretch.model.File;
import com.infostretch.model.Product;
import com.infostretch.model.ProductDTO;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	@Autowired
	FileService fileService;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public ProductDTO addProduct(ProductDTO productDTO) throws IOException {

		Product product = getProductFromProductDTO(productDTO);
		File fileObject = fileService.addFile(productDTO.getImage());
		product.setFile(fileObject);
		product = productDAO.addProduct(product);
		return getProductDTOFromProduct(product);
	}

	@Override
	public ProductDTO getProductById(Integer id) {
		Product product = productDAO.getProductById(id);
		return getProductDTOFromProduct(product);
	}

	@Override
	public List<ProductDTO> getAllProoducts() {
		List<Product> products = productDAO.getAllProducts();

		Collections.sort(products, new Comparator<Product>() {
			@Override
			public int compare(Product o1, Product o2) {
				if (o1.getProductPrice().equals(o2.getProductPrice())
						&& o1.getProductWear().compareTo(o2.getProductWear()) > 0) {
					return 1;
				} else if (o1.getProductPrice() > o2.getProductPrice())
					return 1;
				else
					return -1;
			}
		});

		// Sorting on multiple fields; Group by.
//		Comparator<Product> groupByComparator = Comparator.comparing(Product::getProductPrice)
//				.thenComparing(Product::getProductWear);
//		products.sort(groupByComparator);
		return getProductDTOsFromProducts(products);
	}

	@Override
	public void deleteProduct(Integer id) {
		productDAO.deleteProduct(id);
	}

	@Override
	public ProductDTO updateProduct(ProductDTO productDTO, Integer id) {
		Product product = getProductFromProductDTO(productDTO);
		product = productDAO.updateProduct(product, id, productDTO.getImage());
		productDTO = getProductDTOFromProduct(product);
		return productDTO;
	}

	@Override
	public List<ProductDTO> getAllProductsSortByPrice(String sortType) {
		List<Product> products = productDAO.getAllProductsSortByPrice(sortType);
		return getProductDTOsFromProducts(products);
	}

	@Override
	public List<ProductDTO> getAllProductsByColor(String color) {
		List<Product> products = productDAO.getAllProductsByColor(color);
		return getProductDTOsFromProducts(products);
	}

	@Override
	public List<ProductDTO> getAllProductsByGender(String gender) {
		List<Product> products = productDAO.getAllProductsByGender(gender);
		return getProductDTOsFromProducts(products);
	}

	@Override
	public List<String> getColors() {
		return productDAO.getColors();
	}

	@Override
	public List<ProductDTO> getAllProductsByProductWear(String wear) {
		List<Product> products = productDAO.getAllProductsByWear(wear);
		return getProductDTOsFromProducts(products);
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// Below are the methods for conversion / Mapper
	@Override
	public Product getProductFromProductDTO(ProductDTO productDTO) {
		Product product = modelMapper.map(productDTO, Product.class);
		return product;
	}

	@Override
	public ProductDTO getProductDTOFromProduct(Product product) {
		ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
		productDTO.setImageData(product.getFile().getFileData());
		return productDTO;
	}

	@Override
	public List<ProductDTO> getProductDTOsFromProducts(List<Product> products) {
		List<ProductDTO> productDTOs = products.stream().map(product -> modelMapper.map(product, ProductDTO.class))
				.collect(Collectors.toList());
		return productDTOs;
	}
}
