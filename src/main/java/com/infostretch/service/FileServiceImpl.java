package com.infostretch.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.infostretch.DAO.FileDAO;
import com.infostretch.model.File;

@Service
public class FileServiceImpl implements FileService {

	@Autowired
	FileDAO fileDAO;

	@Override
	public File addFile(MultipartFile file) {

		try {
			return fileDAO.addFile(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}