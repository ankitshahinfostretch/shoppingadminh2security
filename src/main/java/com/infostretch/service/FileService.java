package com.infostretch.service;

import org.springframework.web.multipart.MultipartFile;

import com.infostretch.model.File;

public interface FileService {

	public File addFile(MultipartFile file);
}
