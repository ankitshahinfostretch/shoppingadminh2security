package com.infostretch.service;

import java.io.IOException;
import java.util.List;

import com.infostretch.model.Product;
import com.infostretch.model.ProductDTO;

public interface ProductService {

	ProductDTO addProduct(ProductDTO productDTO) throws IOException;

	public ProductDTO getProductById(Integer id);

	public List<ProductDTO> getAllProoducts();

	public void deleteProduct(Integer id);

	public ProductDTO updateProduct(ProductDTO productDTO, Integer id);

	public List<String> getColors();

	// Below Methods are for sorting

	public List<ProductDTO> getAllProductsSortByPrice(String sortType);

	// Below Methods are for searching

	public List<ProductDTO> getAllProductsByColor(String color);

	public List<ProductDTO> getAllProductsByGender(String gender);
	
	public List<ProductDTO> getAllProductsByProductWear(String gender);

	// Below Methods used for mapper/conversion

	public Product getProductFromProductDTO(ProductDTO productDTO);

	public ProductDTO getProductDTOFromProduct(Product product);

	public List<ProductDTO> getProductDTOsFromProducts(List<Product> products);
}
