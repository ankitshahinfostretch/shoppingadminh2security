package com.infostretch.controllers;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.model.ProductDTO;
import com.infostretch.service.ProductService;

@RestController
public class ProductController {

	Logger log = Logger.getLogger(getClass().getName());

	@Autowired
	ProductService productService;

	@GetMapping("/")
	public ModelAndView indexPage() {
		return new ModelAndView("redirect:/products");
	}

	@GetMapping("/add")
	public ModelAndView getAddProductForm() {
		String[] genders = { "Male", "Female" };
		ModelAndView modelAndView = new ModelAndView("ProductForm", "product", new ProductDTO());
		modelAndView.addObject("genders", genders);
		return modelAndView;
	}

	// To get Edit form
	@GetMapping("/products/{id}")
	public ModelAndView getEditProductForm(@PathVariable("id") Integer id) {
		String[] genders = { "Male", "Female" };
//		ProductDTO productDTO = productService.getProductById(id);
		ModelAndView modelAndView = new ModelAndView("editProduct", "product", productService.getProductById(id));
		modelAndView.addObject("genders", genders);
		return modelAndView;
	}

	@PostMapping("/add")
	public ModelAndView addProduct(@ModelAttribute(name = "product") ProductDTO productDTO) throws IOException {
		productService.addProduct(productDTO);
		return new ModelAndView("redirect:/products");
	}

	@GetMapping("/products")
	public ModelAndView getAllProducts() {
		return new ModelAndView("products", "products", productService.getAllProoducts());
	}

	@GetMapping("/products/{id}/getImage")
	public @ResponseBody ResponseEntity<byte[]> getImage(@PathVariable("id") Integer id) {
		byte[] bytes = productService.getProductById(id).getImageData();
		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(bytes);
	}

	@DeleteMapping("/products/{id}")
	public ModelAndView deleteProduct(@PathVariable("id") Integer id) {
		productService.deleteProduct(id);
		return new ModelAndView("redirect:/products");
	}

	// For Update
	@PostMapping("/products/{id}")
	public ModelAndView updateProduct(@ModelAttribute(name = "productDTO") ProductDTO productDTO,
			@PathVariable("id") Integer id) {
		productService.updateProduct(productDTO, id);
		return new ModelAndView("redirect:/products");
	}

	@GetMapping("/products/price/{sortType}")
	public ModelAndView sortProductsByPrice(@PathVariable("sortType") String sortType) {
		return new ModelAndView("products", "products", productService.getAllProductsSortByPrice(sortType));
	}

	@GetMapping("/products/wear/{sortType}")
	public ModelAndView sortProductsByWear(@PathVariable("sortType") String wear) {
		return new ModelAndView("products", "products", productService.getAllProductsByProductWear(wear));
	}

	@GetMapping("/sort")
	public ModelAndView getSortPage() {
		ModelAndView modelAndView = new ModelAndView("sortProduct");
		String[] genders = { "Male", "Female" };
		modelAndView.addObject("genders", genders);
		String[] wear = { "Topwear", "Bottomwear", "Footwear" };
		modelAndView.addObject("wears", wear);
		modelAndView.addObject("colors", productService.getColors());
		return modelAndView;
	}

	@GetMapping("/products/color/{color}")
	public ModelAndView searchProdcutByColor(@PathVariable("color") String color) {
		return new ModelAndView("products", "products", productService.getAllProductsByColor(color));
	}

	@GetMapping("/products/gender/{gender}")
	public ModelAndView searchProdcutByGender(@PathVariable("gender") String gender) {
		return new ModelAndView("products", "products", productService.getAllProductsByGender(gender));
	}

	@GetMapping("/products/wears/{wear}")
	public String[] getWears(@PathVariable("wear") String wear) {
		if (wear.equalsIgnoreCase("Topwear"))
			return new String[] { "Shirt", "Dress", "T-shirt" };
		else if (wear.equalsIgnoreCase("Bottomwear"))
			return new String[] { "Jeans" };
		return new String[] { "Slippers" };
	}

}