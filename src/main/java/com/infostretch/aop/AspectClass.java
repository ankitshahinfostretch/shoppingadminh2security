package com.infostretch.aop;

import java.util.logging.Logger;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AspectClass {

	Logger logger = Logger.getLogger(AspectClass.class.getName());

//	@Before("execution(* com.infostretch.controllers.*.*(..))")
//	public void beforeMethod(JoinPoint jp) {
//		logger.warning("Request Arrived : " + jp.toString());
//	}
}
